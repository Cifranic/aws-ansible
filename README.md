
# Running ansible script
```terminal
ansible-playbook -i hosts 00-deploy-ec2.yml
```

# Check syntax:
```terminal
ansible-playbook 00-deploy-ec2.yml --check
```

# Cloud modules reference:
https://docs.ansible.com/ansible/latest/modules/list_of_cloud_modules.html

# Additional Resource
https://www.freecodecamp.org/news/ansible-manage-aws/


